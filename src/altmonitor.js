const console = require("console");
const browser = require("webextension-polyfill");

/**
 * The actual monitor
 */
class AltMonitor {
    constructor() {
    }

    /**
     * Initializes the altcoin monitor
     */
    init() {
        console.debug("Initializing...");
        this.checkPrice();
        setInterval(() => this.checkPrice(), 5000); //1min
    }

    /**
     * Checks the price for all the configured cryptocurrencies
     */
    checkPrice() {
        console.debug("Checking price...");
        browser.browserAction.setBadgeText({
            text: "Hello"
        });
    }
}

module.exports = AltMonitor;
