"use strict";

const path = require("path");

const watchify = require("watchify");
const browserify = require("browserify");
const vueify = require("vueify");

const gulp = require("gulp");
const zip = require("gulp-zip");
const gutil = require("gulp-util");
const sourcemaps = require("gulp-sourcemaps");
const gsync = require("gulp-sync")(gulp);

const source = require("vinyl-source-stream");
const vbuffer = require("vinyl-buffer");
const del = require("del");

let b = browserify({
    entries: [
        "./src/background.js",
        "./src/main.js"
    ],
    cache: {},
    packageCache: {},
    debug: true
});
b.transform(vueify);

function bundleJs() {
    return b.bundle()
        .on("error", gutil.log.bind(gutil, "Browserify Error"))
        .pipe(source("bundle.js"))
        .pipe(vbuffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./dist"));
}

gulp.task('test', function (done) {
    const Server = require('karma').Server;
    new Server(
        {
            configFile: path.join(__dirname, "karma.conf.js"),
            singleRun: true
        }
        , done)
        .start();
});

gulp.task("js", bundleJs);

gulp.task("build", gsync.sync([
    "clean"
    , [
        //async
        "js"
    ]
], "build-group"));

gulp.task("dev", function (done) {
    b = watchify(b);

    b.on("update", bundleJs);
    b.on("log", gutil.log);

    //only exits this task when hit ctrl-c
    process.on("SIGINT", () => {
        gutil.log("Stopping watcher...");
        b.close();
        done();
        process.exit(0);
    });

    bundleJs();
});

gulp.task("export-xpi", function () {
    let zipFiles = [
        "./dist/**"
        , "manifest.json"
    ];
    return gulp.src(zipFiles)
        .pipe(zip("AltMonitor.xpi"))
        .pipe(gulp.dest("."));
});

gulp.task("clean", function () {
    return del("./dist/**");
});
